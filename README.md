## Introduction

railsdev-skeleton builds an environment to install Ruby on Rails application without making any changes to your machine.

* Open `ansible/group_vars/all` and change the `ruby_version` value to the version of ruby you want by default.

After making the changes mentioned above execute following commnad. Please note that depending on your download speed it could take upto 15 mintues for the guest machine to be built. During the installation process you will see lots of outputs because the script has been set to super verbose mode to show all the data it can.

```
vagrant up
```

Execute following commands to get into the guest machine.

```
vagrant ssh
```

```
cd /vagrant
gem install bundler
bundle install
```

```
./bin/rails server -b 0.0.0.0
```

Now open browser and visit `http://localhost:3000`.

### MySQL

  * Users:
    * root / password
    * railsdev / password
